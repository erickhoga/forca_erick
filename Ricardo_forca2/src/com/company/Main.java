package com.company;

import java.io.IOException;
import java.net.*;
import java.io.*;
import java.util.Scanner;

public class Main {
    public static boolean continua = true;
    public static void main(String[] args) throws IOException{
        while(continua == true){
            Scanner teclado = new Scanner(System.in);
            String fala = "";
            Socket socket = new Socket("localhost", 1005);
            PrintWriter oquevai = new PrintWriter(socket.getOutputStream());

            BufferedReader oqveio = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            System.out.println(oqveio.readLine());


            fala = teclado.nextLine();
            while (eletra(fala) == false){
                System.out.println("Valor inválido, tente novamente:");
                fala = teclado.nextLine();
            }

            if(fala.equals("sair")){
                System.exit(0);
            }
            oquevai.write(fala);


            oquevai.close();
            oqveio.close();
            socket.close();
        }
    }

    public static boolean eletra(String x){
        if(x.equals("sair")){
            return true;
        }
        if(x.length()==1 && Character.isLetter(x.charAt(0))){
            return true;
        }
        return false;
    }
}
