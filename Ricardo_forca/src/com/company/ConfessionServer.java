package com.company;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.Scanner;

public class ConfessionServer {

    public static int ponto;
    public static int pontoMenos;
    public static int pontoMais;
    public static String letrasQueJaForam = "";
    public static int letraRepetida;

    public static void main(String... args) throws IOException {

        int x = 0;
        String texto = "";
        String dir = System.getProperty("user.dir");
        String diretorio = dir+"\\"+"Palavras.txt";
        String todasPalavras = "";

        try {
            FileReader arq = new FileReader(diretorio);
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine();
            while (linha != null) {
                texto = linha;
                linha = lerArq.readLine();
                todasPalavras = todasPalavras+" "+texto;
            }
            arq.close();
        } catch (IOException e) {
            System.err.println("Erro na abertura do arquivo: exe,p"+e.getMessage());
        }
        int inicio = 0;

        String mensagem = "";
        String palavraOculta;
        String palavra = "";
        String[] palavras = todasPalavras.split(" ");
        while (x == 0){
            x = (int)(Math.floor((Math.random()*palavras.length)));
        }

        palavra = palavras[x];
//        while (epalavra(palavra) == false) {
//            System.out.println("Digite uma palavra válida");
//            palavra = teclado.nextLine();
//        }
        palavraOculta = criarPalavra(palavra);
        while(true){
            ServerSocket endereco = new ServerSocket(1005);
            Socket socket = endereco.accept();
            PrintWriter oquevai = new PrintWriter(socket.getOutputStream());
            BufferedReader oqveio = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String frase = "...";


            if(inicio == 0){
                frase = "Bem vindo ao jogo da forca. A palavra é "+palavraOculta+" (Digite uma letra)";
                inicio++;
            }
            else{
                if(letraRepetida > 0){
                    mensagem = "Letra repetida -1 ponto";
                    ponto = ponto -1;
                }
                if(pontoMais > 0){
                    mensagem = "Você ganhou "+pontoMais+" pontos";
                    ponto = ponto + pontoMais;
                }
                if(pontoMenos > 0){
                    mensagem = "Você perdeu 3 pontos";
                    ponto = ponto - 3;
                }
                if(Acabou(palavraOculta)){
                    mensagem = "Acabou!! Você ganha +5 pontos";
                    ponto = ponto + 5;
                }
                frase = "Digite uma letra para a palavra: "+palavraOculta +"| "+ mensagem + "| Seu total de pontos = "+ponto;
                pontoMenos = 0;
                pontoMais = 0;
                letraRepetida = 0;
            }
            oquevai.println(frase);
            oquevai.flush();

            String oqvem = (oqveio.readLine());
            System.out.println(oqvem);



            endereco.close();

            palavraOculta = modifica(palavra,palavraOculta,oqvem);
            letrasQueJaForam = letrasQueJaForam+oqvem;
        }
    }

    public static String criarPalavra(String x){
        String oculto = "";
        for(int i = 0; i<x.length();i++){
            oculto = oculto+"_";
        }
        return oculto;
    }

    public static boolean repetida(String x, String letra){
        String[] letras = x.split("");
        for(int i = 0; i<letras.length;i++){
            if(letras[i].toUpperCase().equals(letra.toUpperCase())){
                return true;
            }
        }
        return false;
    }

//    public static boolean epalavra(String x){
//        int contador = 0;
//        String[] letras = x.split("");
//        for(int i = 0; i<letras.length;i++){
//            if(Character.isLetter(letras[i].charAt(0))){
//                contador++;
//            }
//        }
//        if(letras.length == contador){
//            return true;
//        }
//        else {
//            return false;
//        }
//    }

    public static boolean Acabou(String palavra){
        int contador = 0;
        String[] letras = palavra.split("");
        for(int i = 0; i<letras.length;i++){
            if(letras[i].equals("_")){
                contador++;
            }
        }
        if(contador == 0){
            return true;
        }
        return false;
    }

    public static String modifica(String palavra, String oculto , String letra){

        String[] letras = palavra.split("");
        String[] letrasOcultas = oculto.split("");
        for(int i = 0; i<letras.length;i++){
            if(letras[i].toUpperCase().equals(letra.toUpperCase())){
                letrasOcultas[i] = letra;
                pontoMais++;
            }
        }
        if(pontoMais == 0){
            pontoMenos++;
        }
        if(repetida(letrasQueJaForam,letra)){
            pontoMais = 0;
            pontoMenos = 0;
            letraRepetida++;
        }
        oculto = "";
        for(int i = 0; i<letrasOcultas.length;i++){
            oculto = oculto+letrasOcultas[i];
        }
        return oculto;
    }
}